package az.ibar.taskify.mapper;

import az.ibar.taskify.dao.entity.Task;
import az.ibar.taskify.domain.dto.TaskDto;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-11-27T10:45:42+0400",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 11.0.9.1 (Ubuntu)"
)
public class TaskMapperImpl extends TaskMapper {

    @Override
    public Task toEntity(TaskDto taskDto) {

        Task task = new Task();

        if ( taskDto != null ) {
            if ( taskDto.getTitle() != null ) {
                task.setTitle( taskDto.getTitle() );
            }
            if ( taskDto.getDescription() != null ) {
                task.setDescription( taskDto.getDescription() );
            }
            if ( taskDto.getDeadline() != null ) {
                task.setDeadline( taskDto.getDeadline() );
            }
        }

        return task;
    }
}
