package az.ibar.taskify.mapper;

import az.ibar.taskify.dao.entity.Customer;
import az.ibar.taskify.dao.entity.User;
import az.ibar.taskify.domain.dto.RegistrationDto;
import az.ibar.taskify.domain.dto.UserDto;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-11-27T10:45:42+0400",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 11.0.9.1 (Ubuntu)"
)
public class UserMapperImpl extends UserMapper {

    @Override
    public User toEntity(RegistrationDto registrationDto) {

        User user = new User();

        if ( registrationDto != null ) {
            user.setCustomer( registrationDtoToCustomer( registrationDto ) );
            if ( registrationDto.getLastName() != null ) {
                user.setLastName( registrationDto.getLastName() );
            }
            if ( registrationDto.getPassword() != null ) {
                user.setPassword( registrationDto.getPassword() );
            }
            if ( registrationDto.getName() != null ) {
                user.setName( registrationDto.getName() );
            }
            if ( registrationDto.getEmail() != null ) {
                user.setEmail( registrationDto.getEmail() );
            }
            if ( registrationDto.getUsername() != null ) {
                user.setUsername( registrationDto.getUsername() );
            }
        }

        return user;
    }

    @Override
    public User toEntity(UserDto userDto) {

        User user = new User();

        if ( userDto != null ) {
            if ( userDto.getName() != null ) {
                user.setName( userDto.getName() );
            }
            if ( userDto.getLastName() != null ) {
                user.setLastName( userDto.getLastName() );
            }
            if ( userDto.getPassword() != null ) {
                user.setPassword( userDto.getPassword() );
            }
            if ( userDto.getEmail() != null ) {
                user.setEmail( userDto.getEmail() );
            }
            if ( userDto.getUsername() != null ) {
                user.setUsername( userDto.getUsername() );
            }
        }

        return user;
    }

    protected Customer registrationDtoToCustomer(RegistrationDto registrationDto) {

        Customer customer = new Customer();

        if ( registrationDto != null ) {
            if ( registrationDto.getOrganizationName() != null ) {
                customer.setOrganizationName( registrationDto.getOrganizationName() );
            }
            if ( registrationDto.getPhoneNumber() != null ) {
                customer.setPhoneNumber( registrationDto.getPhoneNumber() );
            }
            if ( registrationDto.getAddress() != null ) {
                customer.setAddress( registrationDto.getAddress() );
            }
        }

        return customer;
    }
}
