package az.ibar.taskify.service.Impl;

import az.ibar.taskify.dao.entity.Customer;
import az.ibar.taskify.dao.entity.Role;
import az.ibar.taskify.dao.entity.User;
import az.ibar.taskify.dao.repository.RoleRepository;
import az.ibar.taskify.dao.repository.UserRepository;
import az.ibar.taskify.domain.dto.RegistrationDto;
import az.ibar.taskify.mapper.UserMapper;
import az.ibar.taskify.service.Impl.RegistrationServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class RegistrationServiceImplTest {

    private static final Long DUMMY_ID = 1L;
    private static final String DUMMY_ROLE_ADMIN = "ADMIN";

    @InjectMocks
    private RegistrationServiceImpl registrationService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private RoleRepository roleRepository;

    @Spy
    private UserMapper userMapper;

    private RegistrationDto registrationDto;
    private Role role;
    private Customer customer;
    private User user;


    @BeforeEach
    void setUp(){
        registrationDto = RegistrationDto
                .builder()
                .build();

        customer = Customer.builder().build();

        role = Role
                .builder()
                .id(DUMMY_ID)
                .roleName(DUMMY_ROLE_ADMIN)
                .build();

        user = User
                .builder()
                .customer(customer)
                .role(role)
                .build();
    }

    @Test
    void givenRegistrationDtoWhenSignUpThenSave(){
        //Arrange
        when(roleRepository.findByRoleName(DUMMY_ROLE_ADMIN)).thenReturn(role);

        //Act
        registrationService.signUp(registrationDto);

        //Verify
        verify(userRepository,times(1)).save(user);
    }
}
