package az.ibar.taskify.service.Impl;

import static org.assertj.core.api.Assertions.assertThatThrownBy;


import az.ibar.taskify.dao.entity.Customer;
import az.ibar.taskify.dao.entity.Role;
import az.ibar.taskify.dao.entity.User;
import az.ibar.taskify.dao.repository.RoleRepository;
import az.ibar.taskify.dao.repository.UserRepository;
import az.ibar.taskify.domain.dto.UserDto;
import az.ibar.taskify.exception.AuthException;
import az.ibar.taskify.exception.UserNotFoundException;
import az.ibar.taskify.mapper.UserMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;


import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

    private static final Long DUMMY_ID = 1L;
    private static final String DUMMY_STRING = "string";
    private static final String DUMMY_ROLE_ADMIN = "ADMIN";
    private static final String DUMMY_ROLE_USER = "USER";


    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private RoleRepository roleRepository;

    @Spy
    private UserMapper userMapper;

    private UserDto userDto;
    private User user;
    private Role role;
    private Customer customer;

    @BeforeEach
    void setUp(){

        role = Role
                .builder()
                .id(DUMMY_ID)
                .roleName(DUMMY_ROLE_ADMIN)
                .build();

        customer = Customer
                .builder()
                .id(DUMMY_ID)
                .build();

        user = User
                .builder()
                .name(DUMMY_STRING)
                .lastName(DUMMY_STRING)
                .username(DUMMY_STRING)
                .password(DUMMY_STRING)
                .email(DUMMY_STRING)
                .role(role)
                .customer(customer)
                .build();

        userDto = UserDto
                .builder()
                .name(DUMMY_STRING)
                .lastName(DUMMY_STRING)
                .username(DUMMY_STRING)
                .password(DUMMY_STRING)
                .email(DUMMY_STRING)
                .build();
    }

    @Test
    void givenTokenIsNotPresentWhenGetCustomerThenException(){
        //Arrange
        when(userRepository.findByToken(DUMMY_STRING)).thenReturn(Optional.empty());

        //Act & Assert
        assertThatThrownBy(()->userService.createUser(userDto,DUMMY_STRING)).isInstanceOf(UserNotFoundException.class);
    }

    @Test
    void givenTokenIsPresentAndUserIsAdminWhenGetCustomerThenException(){
        //Arrange
        when(userRepository.findByToken(DUMMY_STRING)).thenReturn(Optional.of(user));
        Role role = Role.builder().id(DUMMY_ID).roleName("User").build();
        user.setRole(role);

        //Act & Assert
        assertThatThrownBy(()->userService.createUser(userDto,DUMMY_STRING)).isInstanceOf(AuthException.class);
    }

    @Test
    void givenValidTokenAndUserDtoWhenCreateUserThenSave(){
        //Arrange
        when(userRepository.findByToken(DUMMY_STRING)).thenReturn(Optional.of(user));
        when(roleRepository.findByRoleName(DUMMY_ROLE_USER)).thenReturn(role);

        //Act
        userService.createUser(userDto,DUMMY_STRING);

        //Verify
        verify(userRepository,times(1)).save(user);
    }
}
