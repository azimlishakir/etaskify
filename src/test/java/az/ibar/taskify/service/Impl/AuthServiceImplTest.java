package az.ibar.taskify.service.Impl;

import az.ibar.taskify.dao.entity.User;
import az.ibar.taskify.dao.repository.UserRepository;
import az.ibar.taskify.domain.dto.AuthDto;
import az.ibar.taskify.domain.dto.AuthRespDto;
import az.ibar.taskify.exception.UserNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.InstanceOfAssertFactories.OPTIONAL;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AuthServiceImplTest {

    private static final String DUMMY_STRING = "string";

    @InjectMocks
    private AuthServiceImpl authService;

    @Mock
    private UserRepository userRepository;

    private AuthDto authDto;
    private User user;
    private AuthRespDto authRespDto;

    @BeforeEach
    void setUp() {
        authDto = AuthDto
                .builder()
                .username(DUMMY_STRING)
                .password(DUMMY_STRING)
                .build();

        user = User.builder()
                .token(DUMMY_STRING)
                .username(DUMMY_STRING)
                .password(DUMMY_STRING)
                .build();
        authRespDto = new AuthRespDto(UUID.randomUUID().toString());
    }

    @Test
    void givenUsernameOrPasswordIsNotPresentWhenLoginThenException() {
        //Arrange
        when(userRepository.findByUsernameAndPassword(authDto.getUsername(), authDto.getPassword())).thenReturn(Optional.empty());

        //Act & Assert
        assertThatThrownBy(() -> authService.login(authDto)).isInstanceOf(UserNotFoundException.class);
    }

    @Test
    void givenUsernameAndPasswordIsPresentWhenLoginThenSave() {
        //Arrange
        when(userRepository.findByUsernameAndPassword(authDto.getUsername(),authDto.getPassword())).thenReturn(Optional.of(user));

        //Act
        authService.login(authDto);

        //Verify
        verify(userRepository,times(1)).save(user);
    }

    @Test
    void givenTokenIsNotPresentWhenHasValidTokenThenFalse(){
        //Arrange
        when(userRepository.findByToken(DUMMY_STRING)).thenReturn(Optional.empty());
        //Act & Assert
        assertThat(authService.hasValidToken(DUMMY_STRING)).isEqualTo(false);


    }
}
