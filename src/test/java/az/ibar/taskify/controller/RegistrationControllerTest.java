package az.ibar.taskify.controller;


import az.ibar.taskify.dao.entity.Customer;
import az.ibar.taskify.dao.entity.Role;
import az.ibar.taskify.dao.entity.User;
import az.ibar.taskify.dao.repository.UserRepository;
import az.ibar.taskify.domain.dto.RegistrationDto;
import az.ibar.taskify.mapper.UserMapper;
import az.ibar.taskify.service.RegistrationService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(SpringExtension.class)
@WebMvcTest(RegistrationController.class)
@WithMockUser
public class RegistrationControllerTest {

    private static final Long DUMMY_ID = 1L;
    private static final String DUMMY_ROLE_ADMIN = "ADMIN";

    @Autowired
    private MockMvc mvc;


    @MockBean
    private RegistrationService registrationService;



    @Autowired
    private ObjectMapper objectMapper;

    private RegistrationDto registrationDto;
    private Role role;
    private Customer customer;
    private User user;

    @BeforeEach
    void setUp(){
        registrationDto = RegistrationDto
                .builder()
                .username("username")
                .address("adress")
                .password("password")
                .build();

        customer = Customer.builder().build();

        role = Role
                .builder()
                .id(DUMMY_ID)
                .roleName(DUMMY_ROLE_ADMIN)
                .build();

        user = User
                .builder()
                .customer(customer)
                .role(role)
                .build();
    }

    @Test
    void signUp() throws Exception{
        doNothing().when(registrationService).signUp(any());

        mvc.perform(post("/sign-up")
                .content(objAsJson(registrationDto))
                .contentType(APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .accept(APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    private String objAsJson(Object obj) throws JsonProcessingException {
        return objectMapper.writeValueAsString(obj);
    }


}