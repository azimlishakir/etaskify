package az.ibar.taskify.controller;

import az.ibar.taskify.dao.entity.Customer;
import az.ibar.taskify.dao.entity.Role;
import az.ibar.taskify.dao.entity.User;
import az.ibar.taskify.domain.dto.UserDto;
import az.ibar.taskify.service.Impl.UserServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(UserController.class)
@WithMockUser
public class UserControllerTest {

    private static final Long DUMMY_ID = 1L;
    private static final String DUMMY_STRING = "string";
    private static final String DUMMY_ROLE_ADMIN = "ADMIN";
    private static final String DUMMY_ROLE_USER = "USER";


    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private UserServiceImpl userService;


    private UserDto userDto;
    private User user;
    private Role role;
    private Customer customer;

    @BeforeEach
    void setUp(){

        role = Role
                .builder()
                .id(DUMMY_ID)
                .roleName(DUMMY_ROLE_ADMIN)
                .build();

        customer = Customer
                .builder()
                .id(DUMMY_ID)
                .build();

        user = User
                .builder()
                .name(DUMMY_STRING)
                .lastName(DUMMY_STRING)
                .username(DUMMY_STRING)
                .password(DUMMY_STRING)
                .email(DUMMY_STRING)
                .role(role)
                .customer(customer)
                .build();

        userDto = UserDto
                .builder()
                .name(DUMMY_STRING)
                .lastName(DUMMY_STRING)
                .username(DUMMY_STRING)
                .password(DUMMY_STRING)
                .email(DUMMY_STRING)
                .build();
    }

    @Test
    void createUser() throws Exception{
        doNothing().when(userService).createUser(any(),anyString());

        mvc.perform(post("/createUser")
                .content(objAsJson(userDto))
                .header("Auth","token")
                .contentType(APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .accept(APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    private String objAsJson(Object obj) throws JsonProcessingException {
        return objectMapper.writeValueAsString(obj);
    }




}