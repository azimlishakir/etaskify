package az.ibar.taskify.controller;

import az.ibar.taskify.dao.entity.User;
import az.ibar.taskify.dao.repository.UserRepository;
import az.ibar.taskify.domain.dto.AuthDto;
import az.ibar.taskify.domain.dto.AuthRespDto;
import az.ibar.taskify.service.AuthService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ExtendWith(SpringExtension.class)
@WebMvcTest(AuthController.class)
@WithMockUser
public class AuthControllerTest {
    private static final String DUMMY_STRING = "string";

    private static final String TOKEN = "$.token";
    private static final String API = "/auth";

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AuthService authService;



    @Autowired
    private ObjectMapper objectMapper;




    private AuthDto authDto;

    private User user;
    private AuthRespDto authRespDto;

    @BeforeEach
    void setUp() {
        authDto = AuthDto
                .builder()
                .username(DUMMY_STRING)
                .password(DUMMY_STRING)
                .build();

        user = User.builder()
                .token(DUMMY_STRING)
                .username(DUMMY_STRING)
                .password(DUMMY_STRING)
                .build();
        authRespDto = AuthRespDto.builder()
                .token("Some token")
                .build();
    }

    @Test
    void login() throws Exception {
        when(authService.login(any())).thenReturn(authRespDto);
        mvc.perform(post(API)
                .content(objAsJson(authDto))
                .contentType(APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath(TOKEN).value(authRespDto.getToken()));
    }


    private String objAsJson(Object obj) throws JsonProcessingException {
        return objectMapper.writeValueAsString(obj);
    }



}