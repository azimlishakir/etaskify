package az.ibar.taskify.mapper;

import az.ibar.taskify.dao.entity.User;
import az.ibar.taskify.domain.dto.RegistrationDto;
import az.ibar.taskify.domain.dto.UserDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.factory.Mappers;


@Mapper(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public abstract class UserMapper {

    public static final UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

     @Mappings({@Mapping(target = "name",source = "registrationDto.name"),
              @Mapping(target = "lastName",source = "registrationDto.lastName"),
              @Mapping(target = "username",source = "registrationDto.username"),
              @Mapping(target = "password",source = "registrationDto.password"),
              @Mapping(target = "email",source = "registrationDto.email"),
              @Mapping(target = "customer.phoneNumber",source = "registrationDto.phoneNumber"),
              @Mapping(target = "customer.organizationName",source = "registrationDto.organizationName"),
              @Mapping(target = "customer.address",source = "registrationDto.address")})
     public abstract User toEntity(RegistrationDto registrationDto);



    @Mappings({@Mapping(target = "name",source = "userDto.name"),
            @Mapping(target = "lastName",source = "userDto.lastName"),
            @Mapping(target = "username",source = "userDto.username"),
            @Mapping(target = "password",source = "userDto.password"),
            @Mapping(target = "email",source = "userDto.email")})
    public abstract User toEntity(UserDto userDto);


}
