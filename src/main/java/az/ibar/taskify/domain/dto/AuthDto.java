package az.ibar.taskify.domain.dto;


import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
@ToString
public class AuthDto {

    private String username;
    private String password;

}
