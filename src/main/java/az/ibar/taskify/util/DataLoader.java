package az.ibar.taskify.util;

import az.ibar.taskify.dao.entity.Role;
import az.ibar.taskify.dao.repository.RoleRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DataLoader implements CommandLineRunner {

    private RoleRepository roleRepository;

    public DataLoader(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        Role role = new Role();
        role.setRoleName("ADMIN");
        roleRepository.save(role);

        //

        Role role1 = new Role();
        role1.setRoleName("USER");
        roleRepository.save(role1);
    }
}
