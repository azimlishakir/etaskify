package az.ibar.taskify.service.Impl;

import az.ibar.taskify.dao.entity.Customer;
import az.ibar.taskify.dao.entity.Role;
import az.ibar.taskify.dao.entity.User;
import az.ibar.taskify.dao.repository.RoleRepository;
import az.ibar.taskify.dao.repository.UserRepository;
import az.ibar.taskify.domain.dto.UserDto;
import az.ibar.taskify.exception.AuthException;
import az.ibar.taskify.exception.UserNotFoundException;
import az.ibar.taskify.mapper.UserMapper;
import az.ibar.taskify.service.UserService;
import org.springframework.stereotype.Service;


@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private RoleRepository roleRepository;

    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public void createUser(UserDto userDto, String token) {
        User user = UserMapper.INSTANCE.toEntity(userDto);
        Role role = roleRepository.findByRoleName("USER");
        user.setRole(role);
        user.setCustomer(getCustomer(token));
        userRepository.save(user);
    }

    private Customer getCustomer(String token) {
        User user = userRepository
                .findByToken(token)
                .orElseThrow(()->new UserNotFoundException("user not found","exception.user-not-found"));

        if (!user.getRole().getRoleName().equals("ADMIN")) {
            throw new AuthException("access denied","exception.access-denied");
        }

        return user.getCustomer();
    }
}
