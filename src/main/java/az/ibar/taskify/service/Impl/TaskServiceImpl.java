package az.ibar.taskify.service.Impl;

import az.ibar.taskify.dao.entity.Task;
import az.ibar.taskify.dao.repository.TaskRepository;
import az.ibar.taskify.dao.repository.UserRepository;
import az.ibar.taskify.domain.dto.TaskDto;
import az.ibar.taskify.domain.enums.MailSendStatus;
import az.ibar.taskify.mapper.TaskMapper;
import az.ibar.taskify.service.TaskService;
import org.springframework.stereotype.Service;

@Service
public class TaskServiceImpl implements TaskService {

    private TaskRepository taskRepository;
    private UserRepository userRepository;

    public TaskServiceImpl(TaskRepository taskRepository, UserRepository userRepository) {
        this.taskRepository = taskRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void createTask(TaskDto taskDto, String token) {
        taskDto.getAssignIds().forEach(id -> {
            Task task = TaskMapper.INSTANCE.toEntity(taskDto);
            task.setReporterUser(userRepository.findByToken(token).get());
            task.setAssignId(id);
            task.setSendStatus(MailSendStatus.NOT_SENDED);
            taskRepository.save(task);
        });

    }
}
