package az.ibar.taskify.service;

import az.ibar.taskify.domain.dto.UserDto;

public interface UserService {
    void createUser(UserDto userDto, String token);
}
