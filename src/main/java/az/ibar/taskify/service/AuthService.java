package az.ibar.taskify.service;


import az.ibar.taskify.domain.dto.AuthDto;
import az.ibar.taskify.domain.dto.AuthRespDto;

public interface AuthService {

    AuthRespDto login(AuthDto authDto);

    boolean hasValidToken(String token);

}
