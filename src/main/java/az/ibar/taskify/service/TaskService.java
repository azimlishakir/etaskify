package az.ibar.taskify.service;

import az.ibar.taskify.domain.dto.TaskDto;

public interface TaskService {

    void createTask(TaskDto taskDto, String token);

}
