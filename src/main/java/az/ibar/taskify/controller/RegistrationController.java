package az.ibar.taskify.controller;

import az.ibar.taskify.domain.dto.RegistrationDto;
import az.ibar.taskify.service.RegistrationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RegistrationController {

    private RegistrationService service;

    public RegistrationController(RegistrationService service) {
        this.service = service;
    }

    @PostMapping("/sign-up")
    public ResponseEntity signUp(@RequestBody RegistrationDto registrationDto) {
        service.signUp(registrationDto);
        return ResponseEntity.ok().build();
    }

}
