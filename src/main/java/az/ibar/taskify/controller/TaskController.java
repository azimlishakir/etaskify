package az.ibar.taskify.controller;

import az.ibar.taskify.domain.dto.TaskDto;
import az.ibar.taskify.service.TaskService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;

@RestController
public class TaskController {

    private TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping("/create-task")
    @PreAuthorize("@authServiceImpl.hasValidToken(#token)")
    public ResponseEntity createTask(@RequestBody TaskDto taskDto,
                                     @RequestHeader("Auth") @NotBlank String token) {
        taskService.createTask(taskDto, token);
        return ResponseEntity.ok().build();
    }


}
