package az.ibar.taskify.dao.repository;

import az.ibar.taskify.dao.entity.Task;
import az.ibar.taskify.domain.enums.MailSendStatus;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TaskRepository extends CrudRepository<Task,Long> {
    List<Task> findBySendStatus(MailSendStatus mailSendStatus);
}
