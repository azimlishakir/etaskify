package az.ibar.taskify.dao.repository;


import az.ibar.taskify.dao.entity.Role;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface RoleRepository extends CrudRepository<Role,Long> {
    Role findByRoleName(String roleName);
}
